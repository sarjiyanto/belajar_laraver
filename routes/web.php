<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

route::get('/master', function () {
    return view('master');
});

route::get('/data', function () {
    return view('data');
});

route::get('/pertanyaan', 'pertanyaanController@index');

route::get('/pertanyaan/create', 'pertanyaanController@create');

route::post('/pertanyaan', 'pertanyaanController@store');

route::get('/pertanyaan/{pertanyaan_id}', 'pertanyaanController@show');

route::get('/pertanyaan/{pertanyaan_id}/edit', 'pertanyaanController@edit');

route::put('/pertanyaan/{pertanyaan_id}', 'pertanyaanController@update');

route::delete('/pertanyaan/{pertanyaan_id}', 'pertanyaanController@destroy');
