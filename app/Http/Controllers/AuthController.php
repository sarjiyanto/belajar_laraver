<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }


    public function welcome(Request $request)
    {
        //  ($request->all());

        $nama = $request->fname;
        $nama2 = $request->lname;
        return view('welcome', compact('nama', 'nama2'));
        // return "$nama $nama2";
    }
}
