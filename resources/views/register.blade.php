<!DOCTYPE html>
<html>

<head>
    <title>form</title>
</head>

<body>

    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name : </label> <br>
        <br>
        <input type="text" id="fname" name="fname" value="Isi nama depan"> <br>
        <br>
        <label for="lname">Last Name :</label> <br>
        <br>
        <input type="text" name="lname" id="lname" value="Isi nama belakang"> <br>
        <br>
        <label for="gender">Gender :</label> <br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <br>
        <label for="nation">Nationality</label> <br>
        <br>
        <select>
            <option>Indonesian</option>
            <option>Malaysian</option>
            <option>Singaporean</option>
            <option>Australian</option>
        </select>
        <br>
        <br>

        <label for="lang">Languange Spoken :</label> <br>
        <br>
        <input type="checkbox" name="bahasa" id="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="english" id="english">English <br>
        <input type="checkbox" name="other" id="other">Other <br>
        <br>

        <label for="bio">Bio</label> <br>
        <textarea cols="35" rows="10"></textarea> <br>

        <input type="submit" name="submit">

    </form>
</body>

</html>