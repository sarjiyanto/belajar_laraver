@extends ('master')
@section ('title', 'Detail pertanyaan')

@section ('content')

<h4>{{ $pertanyaan->judul}}</h4>
<p>{{ $pertanyaan->isi}}</p>
@endsection