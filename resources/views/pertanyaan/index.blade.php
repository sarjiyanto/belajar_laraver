@extends ('master')
@section ('title', 'Pertanyaan')

@section ('content')

<a href="/pertanyaan/create" class="btn btn-primary mb-2">Tambah</a>
<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Judul</th>
            <th scope="col">Isi</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse($pertanyaan as $key => $tanya)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$tanya->judul}}</td>
            <td>{{$tanya->isi}}</td>
            <td style="display: flex;">
                <a href="/pertanyaan/{{$tanya->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/pertanyaan/{{$tanya->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                <form action="/pertanyaan/{{$tanya->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <p>Tidak ada pertanyaan</p>

        @endforelse

    </tbody>

</table>

@endsection