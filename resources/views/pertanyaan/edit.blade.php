@extends ('master')

@section ('content')

<h2>Edit Pertanyaan</h2>
<form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="title">Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" value="{{old('judul', $pertanyaan->judul)}}">
        @error('judul')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Isi</label>
        <input type="text" class="form-control" name="isi" id="isi" value="{{old('isi', $pertanyaan->isi)}}">
        @error('body')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Selesai</button>
</form>
</div>
@endsection